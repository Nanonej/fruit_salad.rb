require 'spec_helper'
require_relative '../fruit_salad'

describe Salad do
  salad = Salad.new
  ret1 = salad.freshest("2021/12/15")
  ret2 = salad.freshest("2022/01/01")

  it "get fridge json" do
    expect(salad.fridge).to be_truthy.and be_a(Array)
  end

  it "parse peremption_date to date" do
    expect(salad.fridge.first["peremption_date"]).to be_a(Date)
  end

  it "set an id" do
    expect(salad.fridge.first["id"]).to eq(1)
  end

  it "return an array" do
    expect(ret1).to be_a(Array)
  end

  it "find fruits and contain a string and an id" do
    el = ret1.first
    expect(el).to be_truthy
    expect(el[0]).to be_a(String)
    expect(el[1]).to be_a(Integer)
  end

  it "return an empty array when all fruits are perempted" do
    expect(ret2.first).to be_nil
  end
end
