# README

## Install dependencies with
```console
$ bundle install
```

## Launch test with
```console
$ bundle exec rake spec
```
