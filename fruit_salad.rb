require 'JSON'
require 'Date'

class Salad
  def initialize
    @fridge = JSON.parse(File.read("./fridge.json"))
    len = @fridge.length
    @fridge.reverse_each do |el|
      begin
        el["id"] = len
        len -= 1
        el["peremption_date"] = Date.parse(el["peremption_date"])
      rescue => exception
        @fridge.delete(el)
      end
    end
  end

  def fridge
    @fridge
  end

  # freshest(date) => array[{mango ; 2},...] make a fruit salad with a fruit of each color and freshest fruits
  def freshest(day)
    picked_colors = []
    ret = []
    day = Date.parse(day)

    fridge = @fridge.sort_by { |el| el["peremption_date"] }
    fridge.reverse_each do |el|
      if day <= el["peremption_date"] && !picked_colors.include?(el["color"])
        ret.push({0 => el["name"], 1 => el["id"]})
        picked_colors.push(el["color"])
      end
    end
    ret
  end
end

puts(Salad.new.freshest("2021/12/15"))
